import 'package:flutter/material.dart';

void main() {
  runApp(Nav2App());
}

class Nav2App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      onGenerateRoute: (settings) {
        // Handle '/'
        if (settings.name == '/') {
          return MaterialPageRoute(builder: (context) => HomeScreen());
        }

        // Handle '/details/:id'
        var uri = Uri.parse(settings.name);
        if (uri.pathSegments.length == 2 &&
            uri.pathSegments.first == 'details') {
          var id = uri.pathSegments[1];
          return MaterialPageRoute(builder: (context) => DetailScreen(id: id));
        }

        return MaterialPageRoute(builder: (context) => UnknownScreen());
      },
    );
  }
}

class HomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Color(0xFF0519F5),
          title: Text('ลงชื่อกิจกรรม'),
        ),
        body: ListView(children: [
          Image.asset(
            'images/001.jpg',
            width: 900,
            height: 600,
          ),
          Text(
            'กิจกรรม กยศ',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            'เข้าสู่ระบบ',
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.grey[5000],
            ),
          ),
          Center(
            child: FlatButton(
              child: Text('ตกลง'),
              color: Color(0xFFDB041A),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  '/details/1',
                );
              },
            ),
          ),
        ]));
  }
}

class DetailScreen extends StatelessWidget {
  String id;

  DetailScreen({
    this.id,
  });

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xFF0519F5),
        title: Text('กิจกรรม กยศ.'),
      ),
      body: ListView(children: [
        Image.asset(
          'images/001.jpg',
          width: 600,
          height: 300,
        ),
        Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                children: [
                  Text('กิจกรรม กยศ : ....................',
                      style: TextStyle(fontSize: 36)),
                ],
              ),
              Row(
                children: [
                  Text(
                    'รายละเอียดของกิจกรรม : ..................................',
                    style: TextStyle(fontSize: 30),
                  )
                ],
              ),
              Row(
                children: [
                  Text(
                    'ชั่วโมงที่ได้รับของ กยศ : .............. ชั่วโมง',
                    style: TextStyle(fontSize: 30),
                  ),
                ],
              ),
              Row(
                children: [
                  Text(
                    'ลงชื่ออาจารย์ผู้ดูแล : .......................',
                    style: TextStyle(fontSize: 30),
                  ),
                ],
              ),
              Container(
                margin: const EdgeInsets.only(bottom: 150),
              ),
              FlatButton(
                child: Text('กลับหน้าหลัก'),
                color: Color(0xFFDB041A),
                onPressed: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
        ),
      ]),
    );
  }
}

class UnknownScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Center(
        child: Text('404!'),
      ),
    );
  }
}
